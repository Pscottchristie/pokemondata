import datetime
import logging

from flask import Flask
from flask_restplus import Resource, Api, fields
import pandas_datareader as pdr
from werkzeug import cached_property
import matplotlib.pyplot as plt
from flask import send_file
from flask import render_template
import requests 
import urllib
from bs4 import BeautifulSoup


LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)

stock = api.model('stock', {
    'ticker': fields.String(required=True, description='Ticker'),
    })

@api.route('/v1/price/<tickers>')
#@api.param('tickers', 'The stock ticker to get a price for')
class Price(Resource):
    def get(self, tickers):
        ticker_list = [x.strip() for x in tickers.split(',')]
        start_date = datetime.datetime.now() - datetime.timedelta(1)
        results = []
        for ticker in ticker_list:
            try:
                df = pdr.get_data_yahoo(ticker, start_date)[['Close']]
            except Exception as ex:
                LOG.error('Error getting data: ' + str(ex))
            
            LOG.error('Recieved price data:')
            LOG.error(str(df))

            df['Date'] = df.index

            results.append({'ticker': ticker, 'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                             'price': df.iloc[-1]['Close']})

        return results



@api.route('/v1/price/<tickers>/<start_date>/<end_date>')
#@api.param('tickers', 'The stock ticker to get a price for')
class Price_Time(Resource):
    def get(self, tickers, start_date, end_date):
        ticker_list = [x.strip() for x in tickers.split(',')]            
        results = []

        for ticker in ticker_list:
            try:
                df = pdr.get_data_yahoo(ticker, start_date, end_date)[['Close']]
            except Exception as ex:
                LOG.error('Error getting data: ' + str(ex))
            
            LOG.error('Recieved price data:')
            LOG.error(str(df))

            df['Date'] = df.index
            df['new_date_column'] = df['Date'].dt.date
            ticker_prices = {}
            for index,item in df.iterrows():
                ticker_prices[item['new_date_column']] = item['Close']

            results.append({'ticker': ticker, 'date': str(start_date) + "  -  " + str(end_date),
                             'price': ticker_prices })
        return results


@api.route('/v1/price/<tickers>/bollinger')
#@api.param('tickers', 'The stock ticker to get a price for')
class Price(Resource):
    def get(self, tickers):
        ticker_list = [x.strip() for x in tickers.split(',')]

        start_date = datetime.datetime.now() - datetime.timedelta(1)
        start_date_minus40 = datetime.datetime.now() - datetime.timedelta(40)
        results = []

        for ticker in ticker_list:
            try:
                df = pdr.get_data_yahoo(ticker, start_date_minus40)[['Close']]
            except Exception as ex:
                LOG.error('Error getting data: ' + str(ex))
            
            LOG.error('Recieved price data:')
            LOG.error(str(df))
            df['Date'] = df.index

            
            df['30d mavg'] = df['Close'].rolling(window=30).mean()
            df['30d std'] = df['Close'].rolling(window=30).std()

            df['Upper Band'] = df['30d mavg'] + (df['30d std'] * 2)
            df['Lower Band'] = df['30d mavg'] - (df['30d std'] * 2)
            
            df['Action'] = 0
            df.loc[ df['Close'] > df['Upper Band'] , 'Action'] = "SELL" 
            df.loc[ df['Close'] < df['Lower Band'] , 'Action'] = "BUY"
            df.loc[ (df['Close'] > df['Lower Band']) &  (df['Close'] < df['Upper Band']), 'Action'] = "HOLD"
            

            df['new_date_column'] = df['Date'].dt.date

            results.append({'ticker': ticker, 'date': str(df.iloc[-1]['new_date_column']),
                             'price': "{:.2f}".format(df.iloc[-1]['Close']), 'Recommended Action': str(df.iloc[-1]['Action'])})

        return results


@api.route('/v1/price/<tickers>/chart')
#@api.param('tickers', 'The stock ticker to get a price for')
class Price(Resource):
    def get(self, tickers):
        ticker_list = [x.strip() for x in tickers.split(',')]
        start_date = datetime.datetime.now() - datetime.timedelta(90)
        results = []
        for ticker in ticker_list:
            try:
                df = pdr.get_data_yahoo(ticker, start_date)[['Close']]
            except Exception as ex:
                LOG.error('Error getting data: ' + str(ex))
            
            LOG.error('Recieved price data:')
            LOG.error(str(df))

            df['Date'] = df.index

            df_plot=df['Close']
            df_plot.plot(figsize =(18,6))
            plt.title( str(ticker) + '1 Quarter Price Chart')
            plt.ylabel('Closing Price')
            plt.savefig('static/images/char.png')

        return send_file('char.png')


@api.route('/v2/<site_name>')
class Request(Resource):
    def get(self, site_name):
        site = 'http://' + str(site_name) + '.com'
        r = requests.get(site).status_code
        if (r == 200):
            return "Site is up!"
        
        return "Site is down!"



@api.route('/stonks')
class Request(Resource):
    def get(self):
        return send_file('static/images/stonks.png')





@api.route('/v1/stock')
class Stock(Resource):
    @api.expect(stock)
    def post(self):
        LOG.error('Received payload:')
        LOG.error(api.payload)

        # store this record in a database

        return {'result': 'success',
                'ticker': api.payload['ticker']}


if __name__ == '__main__':
    app.run(debug=True)
